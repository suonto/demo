# Gitlab demo

Welcome to our new authentication module.

## Building demoapp
```
docker build -t demoapp app
```

## Running demoapp
```
docker run --name app -d -p 80:7272 demoapp
```
