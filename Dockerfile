FROM python:3-alpine

COPY html /app/html
COPY server.py /app/server.py
WORKDIR /app

CMD ./server.py
